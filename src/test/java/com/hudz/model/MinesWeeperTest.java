package com.hudz.model;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class MinesWeeperTest {
    private static MinesWeeper minesWeeper;
    @Parameterized.Parameter(0)
    public static int x;
    @Parameterized.Parameter(1)
    public static int y;
    private char[][] currentField = new char[x][y];
    private double p;

    @Parameterized.Parameters
    @ParameterizedTest
    @ValueSource
    public static Collection<Object[]> getTestData(){
        return Arrays.asList(new Object[][]{
                {100,200},{-1,5},{-100,200}
        });
    }

    @BeforeAll
    public static void initTest() {
        minesWeeper = new MinesWeeper();
        x = 5;
        y = 5;
    }
    @AfterAll
    public static void afterTest() {
        minesWeeper = null;
    }
    @Test
    void setEmptyField() {
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                currentField[i][j] = ' ';
            }
        }
        assertArrayEquals(currentField, minesWeeper.setEmptyField(x,y));
    }

    @Test
    void setMines() {
        p = 0.5;
        int countOfBombs = (int) ((x * y) * p);
        int userFieldCountOfBombs = 0;
        char[][] userField = minesWeeper.setMines(p);
        for (char[] c: userField) {
            for (char c1: c){
                if(c1 == '*') {
                    userFieldCountOfBombs++;
                }
            }
        }
        assertEquals(countOfBombs, userFieldCountOfBombs - 1);
    }
}