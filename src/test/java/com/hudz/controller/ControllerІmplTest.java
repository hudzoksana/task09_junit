package com.hudz.controller;

import com.hudz.model.MinesWeeper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ControllerІmplTest {
    @InjectMocks
    Controller controller = new ControllerІmpl();

    @Mock
    MinesWeeper minesWeeper;

    public static int x = 5;
    public static int y = 5;

    @Test
    void setEmptyField() {
        char[][] emptyField = { {' ',' ',' ',' ',' '},
                                {' ',' ',' ',' ',' '},
                                {' ',' ',' ',' ',' '},
                                {' ',' ',' ',' ',' '},
                                {' ',' ',' ',' ',' '}
                                };
        when(minesWeeper.setEmptyField(x, y)).thenReturn(emptyField);
        assertArrayEquals(minesWeeper.setEmptyField(x,y), controller.setEmptyField(x, y) );
    }

    @Test
    void setMineInField() {
        double p = 0.5;
        int countOfMines = (int) ( x * y * p);
        char[][] testField = new char[x][y];
        double probability = 0;
        int userMinesCounter = 0;
        while (probability <= p) {
            for (int i = 0; i < testField.length; i++) {
                for (int j = 0; j < testField[0].length; j++) {
                    testField[i][j] = '*';
                    userMinesCounter++;
                }
            }
            probability = (double)userMinesCounter / (testField.length * testField[0].length);
        }
        when(minesWeeper.setMines(p)).thenReturn(testField);
        assertEquals(minesWeeper.setMines(p), controller.setMineInField(p));
    }

}