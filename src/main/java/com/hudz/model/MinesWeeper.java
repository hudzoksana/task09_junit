package com.hudz.model;

public class MinesWeeper {
    char[][] field;

    public char[][] getField() {
        return field;
    }

    public char[][] setEmptyField(int sizeX, int sizeY) {
        field = new char[sizeX][sizeY];
        for (int i = 0; i < sizeY; i++) {
            for (int j = 0; j < sizeX; j++) {
                field[i][j] = ' ';
            }
        }
        return field;
    }

    public char[][] setMines(double p) {
        double minesCounter = 0;
        double probability = 0;
        while (probability <= p) {
            int randomX = (int) (Math.random() * field.length);
            int randomY = (int) (Math.random() * field[0].length);
            if (field[randomX][randomY] == ' ') {
                field[randomX][randomY] = '*';
                minesCounter++;
            }
            probability = minesCounter / (field.length * field[0].length);
        }
        return field;
    }

    public char[][] changeEmptyFieldsToNumber() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                if (field[i][j] != '*') {
                    field[i][j] = ((char) getCountOfBombsAround(i, j));
                }
            }
        }
        return field;
    }

    private int getCountOfBombsAround(int x, int y) {
        int bombCounter = 0;
        for (int i = -1; i <= 1; ++i) {
            int xIndex = x + i;
            if (xIndex < 0 || xIndex >= field.length) {
                continue;
            }
            for (int j = -1; j <= 1; ++j) {
                int yIndex = y + j;
                if (yIndex < 0 || yIndex >= field[0].length) {
                    continue;
                }
                if (i == 0 && j == 0) {
                    continue;
                }
                if (this.field[xIndex][yIndex] == '*') {
                    bombCounter++;
                }
            }
        }
        return bombCounter;
    }

    public void printField() {
        System.out.println("Current field:");
        for (char[] c : field) {
            for (char c1 : c) {
                if (c1 != '*' && c1 != ' ') {
                    System.out.print((int) c1 + " ");
                } else {
                    System.out.print(c1 + " ");
                }
            }
            System.out.println();
        }
    }
}
