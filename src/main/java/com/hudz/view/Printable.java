package com.hudz.view;

@FunctionalInterface
public interface Printable {
  void print();
}
