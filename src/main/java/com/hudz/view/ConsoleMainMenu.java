package com.hudz.view;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.ResourceBundle;

public class ConsoleMainMenu extends AbstractMenu {

  private void setMenu() {

    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("Q", bundle.getString("Q"));
  }

  public ConsoleMainMenu() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    methodsMenu = new LinkedHashMap<>();

    methodsMenu.put("1", this::setEmptyField);
    methodsMenu.put("2", this::setMines);
    methodsMenu.put("3", this::printFieldWithNumbers);
  }

  private void setEmptyField() {
    //TODO:checking input while not correct
    int x;
    int y;
    logger.trace("Please input x:");
    x = input.nextInt();
    logger.trace("Please input y:");
    y = input.nextInt();
    controller.setEmptyField(x, y);
    controller.printCurrentField();
  }

  private void setMines() {
    input.useLocale(Locale.US);
    //TODO:checking input while not correct
    logger.trace("Please input probability for mines [0, 1]:");
    double p = input.nextDouble();
    controller.setMineInField(p);
    controller.printCurrentField();
  }
  private void printFieldWithNumbers() {
    controller.getFieldWithNumbers();
    controller.printCurrentField();
  }
  private void internationalizeMenuUkrainian() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuEnglish() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }
}
