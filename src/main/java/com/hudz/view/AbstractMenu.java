package com.hudz.view;

import com.hudz.controller.Controller;
import com.hudz.controller.ControllerІmpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public abstract class AbstractMenu {
    Locale locale;
    ResourceBundle bundle;
    protected Controller controller;
    protected Map<String, String> menu;
    protected Map<String, Printable> methodsMenu;
    protected Scanner input;
    protected final Logger logger = LogManager.getLogger(AbstractMenu.class);

    AbstractMenu() {
        controller = new ControllerІmpl();
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
    }
    protected void outputMenu() {
        logger.trace("\nMENU:");
        for (String str : this.menu.values()) {
            System.out.println(str);
        }
    }
    public void show() {
        String keySubMenu;
        do {
            this.outputMenu();
            logger.trace("Please, select menu point.");
            keySubMenu = input.nextLine().toUpperCase();

            try {
                this.methodsMenu.get(keySubMenu).print();
            } catch (Exception e) {
                logger.trace("not correct");
            }
        } while (!keySubMenu.equals("Q"));
    }
}
