package com.hudz.controller;

import com.hudz.model.MinesWeeper;

public class ControllerІmpl implements Controller {
    private MinesWeeper minesWeeper = new MinesWeeper();
    boolean settedField = false;
    boolean settedMines = false;

    @Override
    public char[][] setEmptyField(int x, int y) {
        if(!settedField) {
            settedField = true;
           return minesWeeper.setEmptyField(x, y);

        } else System.out.println("Field is already setted");
        return null;
    }
    @Override
    public char[][] setMineInField(double p) {
        if(minesWeeper.getField() != null && settedField && !settedMines) {
            settedMines = true;
            return minesWeeper.setMines(p);
        } else {
            System.out.println("Set field first");
        }
        return minesWeeper.setMines(p);
    }
    @Override
    public void getFieldWithNumbers() {
        if (minesWeeper.getField() != null && settedMines) {
            minesWeeper.changeEmptyFieldsToNumber();
        } else {
            System.out.println("Set field with bombs first");
        }
    }
    @Override
    public void printCurrentField() {
        if (minesWeeper.getField() != null) {
            minesWeeper.printField();
        } else {
            System.out.println("Set field first");
        }
    }
}
