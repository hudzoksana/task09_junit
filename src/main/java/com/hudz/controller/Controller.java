package com.hudz.controller;

public interface Controller {

    char[][] setEmptyField(int x, int y);

    char[][] setMineInField(double p);

    void getFieldWithNumbers();

    void printCurrentField();
}
